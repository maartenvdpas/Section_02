/* Header only extenstion to the CppTestFramework, contains all method specialisations required for testing. */

#pragma once

#include <string>
#include "../Section_02/BullCowGame.h"

namespace Microsoft
{
	namespace VisualStudio
	{
		namespace CppUnitTestFramework
		{			
			// TODO add bunch of test utils here
			template<> inline std::wstring ToString<EInputValidity>(const EInputValidity& InputValidity)
			{
				switch (InputValidity)
				{
				case EInputValidity::OK:
					return ToString("EInputValidity::OK");
				case EInputValidity::NOT_ISOGRAM:
					return ToString("EInputValidity::NOT_ISOGRAM");
				case EInputValidity::NOT_LOWERCASE_LATIN:
					return ToString("EInputValidity::NOT_LOWERCASE_LATIN");
				case EInputValidity::LENGTH_DIFFERENCE:
					return ToString("EInputValidity::LENGTH_DIFFERENCE");
				default:
					return ToString("");
				}
			}
		}
	}
}