#include "stdafx.h"
#include "CppUnitTest.h"
#include "../Section_02/BullCowGame.h"

// Required to use the testing framework, all ToString methods for user defined types are here
#include "VisualStudioTestUtils.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using FString = std::string;
using int32 = int;

namespace Section_02_Test
{		
	TEST_CLASS(BullCowGameTest)
	{
	public:
		
		TEST_METHOD(TestValidateInputOfZeroLength)
		{
			FBullCowGame BullCowGame = FBullCowGame("\0");
			Assert::AreEqual(EInputValidity::OK, BullCowGame.ValidateInput("\0"));
		}

		TEST_METHOD(TestValidateInputOfSingleLength)
		{
			FBullCowGame BullCowGame = FBullCowGame("a");
			Assert::AreEqual(EInputValidity::OK, BullCowGame.ValidateInput("d"));
		}

		TEST_METHOD(TestvalidateInputRepeatingLetters)
		{
			FBullCowGame BullCowGame = FBullCowGame("ias");
			Assert::AreEqual(EInputValidity::NOT_ISOGRAM, BullCowGame.ValidateInput("iii"));
		}

		TEST_METHOD(TestValidateInputUppercase)
		{
			FBullCowGame BullCowGame = FBullCowGame("bingo");
			Assert::AreEqual(EInputValidity::NOT_LOWERCASE_LATIN, BullCowGame.ValidateInput("BINGO"));
		}

		TEST_METHOD(TestValidateInputDifferingLength)
		{
			FBullCowGame BullCowGame = FBullCowGame("bingo");
			Assert::AreEqual(EInputValidity::LENGTH_DIFFERENCE, BullCowGame.ValidateInput("bing"));
		}

		TEST_METHOD(TestIsGameInProgressInitially)
		{
			FBullCowGame BullCowGame = FBullCowGame("bingo");
			Assert::IsTrue(BullCowGame.IsGameInProgress());
		}

		TEST_METHOD(TestIsGameWonInitially)
		{
			FBullCowGame BullCowGame = FBullCowGame("bingo");
			Assert::IsFalse(BullCowGame.IsGameWon());
		}


		TEST_METHOD(TestIsGameInProgressAfterMaximumNumberOfTries)
		{
			FBullCowGame BullCowGame = FBullCowGame("bingo");
			do
			{
				BullCowGame.SubmitValidGuess("bongo");
			} while (BullCowGame.GetCurrentNumberOfTries() <= BullCowGame.GetMaximumNumberOfTries());

			Assert::IsFalse(BullCowGame.IsGameInProgress());
		}

		TEST_METHOD(TestIsGameLostAfterMaximumNumberOfIncorrectTries)
		{
			FBullCowGame BullCowGame = FBullCowGame("bingo");
			do
			{
				BullCowGame.SubmitValidGuess("bongo");
			} while (BullCowGame.GetCurrentNumberOfTries() <= BullCowGame.GetMaximumNumberOfTries());

			Assert::IsFalse(BullCowGame.IsGameWon());
		}

		TEST_METHOD(TestSubmitValidGuessIncreasesCurrentTry)
		{
			FBullCowGame BullCowGame = FBullCowGame("bingo");
			int InitialTries = BullCowGame.GetCurrentNumberOfTries();

			BullCowGame.SubmitValidGuess("bongo");
			Assert::AreEqual(InitialTries + 1, BullCowGame.GetCurrentNumberOfTries());
		}

		TEST_METHOD(TestSubmitValidGuessCheckNoBulls)
		{
			FBullCowGame BullCowGame = FBullCowGame("bingo");
			FBullCowStruct Result = BullCowGame.SubmitValidGuess("acdef");
			Assert::AreEqual(0, Result.Bulls);
		}

		TEST_METHOD(TestSubmitValidGuessCheckSingleBullAnywhereInWord)
		{
			FString HiddenWord = "bingo";
			FBullCowGame BullCowGame = FBullCowGame(HiddenWord);
			
			FString BaseGuess = "acdef";
			for (int i = 0; i < (int32) HiddenWord.length(); i++) 
			{
				FString Guess = BaseGuess;
				Guess.replace(i,1, 1, HiddenWord[i]);
				FBullCowStruct Result = BullCowGame.SubmitValidGuess(Guess);
				Assert::AreEqual(1, Result.Bulls);
			}
		}

		TEST_METHOD(TestSubmitValidGuessCheckMultipleBulls)
		{
			FString HiddenWord = "bingo";
			FBullCowGame BullCowGame = FBullCowGame(HiddenWord);

			FString Guess = "acdef";
			for (int i = 0; i < (int32)HiddenWord.length(); i++)
			{
				Guess.replace(i, 1, 1, HiddenWord[i]);
				FBullCowStruct Result = BullCowGame.SubmitValidGuess(Guess);
				Assert::AreEqual(i+1, Result.Bulls);
			}
		}

		TEST_METHOD(TestSubmitValidGuessCheckSingleCowAnyPosition)
		{
			FString HiddenWord = "bingo";
			FBullCowGame BullCowGame = FBullCowGame(HiddenWord);

			for (int32 i = 1; i < (int32)HiddenWord.length(); i++)
			{
				FString Guess = "acdef";
				Guess.replace(i, 1, 1, 'b');
				FBullCowStruct Result = BullCowGame.SubmitValidGuess(Guess);
				Assert::AreEqual(1, Result.Cows);
			}
		}

		TEST_METHOD(TestSubmitValidGuessAllCharactersAreCows)
		{
			FString HiddenWord = "bingo";
			FBullCowGame BullCowGame = FBullCowGame(HiddenWord);
			FBullCowStruct Result = BullCowGame.SubmitValidGuess("ingob");
			Assert::AreEqual((int32) HiddenWord.length(), Result.Cows);
		}

		TEST_METHOD(TestSubmitValidGuessTotalNumberOfBovineIsLessOrEqualToWordLength)
		{
			FString HiddenWord = "bingo";
			FBullCowGame BullCowGame = FBullCowGame(HiddenWord);
			FString Guess = "ingob";
			// Start with all cows, then slowly swap the last letter with the iterated letter, creating a bull
			for (int32 i = 0; i < (int32) HiddenWord.length(); i++)
			{
				std::swap(Guess[i], Guess[HiddenWord.length() - 1]);
				FBullCowStruct Result = BullCowGame.SubmitValidGuess(Guess);
				Assert::AreEqual((int32)HiddenWord.length(), Result.Cows + Result.Bulls);
			}		
		}

		TEST_METHOD(TestSubmitValidGuessAllBullsResultsInGameWon)
		{
			FString HiddenWord = "bingo";
			FBullCowGame BullCowGame = FBullCowGame(HiddenWord);
			BullCowGame.SubmitValidGuess(HiddenWord);

			Assert::IsTrue(BullCowGame.IsGameWon());
		}

		TEST_METHOD(TestSubmitValidGuessAllBullsResultsInGameFinished)
		{
			FString HiddenWord = "bingo";
			FBullCowGame BullCowGame = FBullCowGame(HiddenWord);
			BullCowGame.SubmitValidGuess(HiddenWord);

			Assert::IsFalse(BullCowGame.IsGameInProgress());
		}
	};
}