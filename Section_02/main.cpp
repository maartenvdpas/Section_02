/**
* This is the console executable which makes used of BullCowGame class.
* This acts as the view in an MVC pattern and is responsible for all user
* interaction. For game logic see the BullCowGame file.
*/

#include "pch.h"
#include <iostream>
#include <string>
#include "BullCowGame.h"

// Define aliases for Unreal esque typing
using FText = std::string; // static strings for output towards user
using FString = std::string; // mutable input strings
using int32 = int;

// Function prototypes
void PlayGame();
void Print(FText Text, bool NewLine = true);

bool AskToPlayAgain();
FString QueryInput(FText InputQuery);
FText GetGuessFeedbackMessage(FBullCowStruct Result);
FText GetInputErrorMessage(EInputValidity);
FText GetGameFinisherMessage(bool);
FText GetBullsAndCowsMessage(FBullCowStruct);

int main()
{
	bool bPlayAgain = false;
	do {
		PlayGame();
		bPlayAgain = AskToPlayAgain();
	} 
	while (bPlayAgain);

	return 0;
}

void PlayGame() 
{
	// TODO - pick a random isogram from a collection
	// TODO - add a user selectable difficulty level (easy, normal, hard)
	FBullCowGame BullCowGame = FBullCowGame();
	Print(BullCowGame.GetIntroductionText());

	// Enter the game loop
	do 
	{
		// TODO - Add current try and maximum tries.
		FString Input = QueryInput("Please enter your guess: ");

		EInputValidity InputValidity = BullCowGame.ValidateInput(Input);
		if (InputValidity != EInputValidity::OK)
		{
			// Inform the player of why the input is not ok
			Print(GetInputErrorMessage(InputValidity));
		}
		else
		{
			FBullCowStruct Result = BullCowGame.SubmitValidGuess(Input);
			Print(GetGuessFeedbackMessage(Result));
		}
	} while (BullCowGame.IsGameInProgress());

	Print(GetGameFinisherMessage(BullCowGame.IsGameWon()));
		
	return;
}

void Print(FText Text, bool NewLine)
{
	std::cout << Text;
	if (NewLine)
	{
		std::cout << std::endl;
	}

	return;
}

bool AskToPlayAgain()
{
	FString Response = QueryInput("Do you wish to play again? (y/n) ");
	return Response[0] == 'y' || Response[0] == 'Y';
}

FString QueryInput(FText QueryText)
{
	Print(QueryText, false);
	FString Input = "";
	std::getline(std::cin, Input);
	return Input;
}

FText GetGuessFeedbackMessage(FBullCowStruct Result)
{
	FText TurnFeedBackText = FText("Bulls: ");
	TurnFeedBackText += FText(std::to_string(Result.Bulls));
	TurnFeedBackText += FText(", Cows: ");
	TurnFeedBackText += FText(std::to_string(Result.Cows));
	TurnFeedBackText += "\n";
	return TurnFeedBackText;
}

FText GetGameFinisherMessage(bool IsGameWon)
{
	if (IsGameWon)
	{
		return "Congratulations, you correctly guessed the secret word!";
	}
	else
	{
		return "Sorry, you didn't guess the secret word in time. Better luck next time!";
	}
}

FText GetInputErrorMessage(EInputValidity InputValidity)
{
	switch (InputValidity)
	{
	case EInputValidity::NOT_ISOGRAM:
		return "Your guess is not an isogram.";
	case EInputValidity::NOT_LOWERCASE_LATIN:
		return "Your guess is not in lowercase latin.";
	case EInputValidity::LENGTH_DIFFERENCE:
		return "Your guess differs in length from the hidden isogram.";
	case EInputValidity::OK:
	default:
		return "Nothing is wrong with your input.";
	}
}
