/* Header file for the Bulls and Cows game controller and all associated model types. */

#pragma once

#include <string>

using FString = std::string;
using FText = std::string;
using int32 = int;

/**
 * Represents the validity input provided to the Bulls and Cows game.
 */
enum class EInputValidity
{
	OK,
	NOT_ISOGRAM,
	NOT_LOWERCASE_LATIN,
	LENGTH_DIFFERENCE
};

/**
 * Bulls and Cows model object, represents the number of bulls and cows in a submitted guess.
 */
struct FBullCowStruct
{
	int Bulls	= 0;
	int Cows	= 0;
};

/**
 * Controller which represents a Bulls and Cows game.
 * 
 * The purpose of the game is to guess the hidden word within the allotted number of tries. 
 */
class FBullCowGame
{
public:
	FBullCowGame();
	FBullCowGame(FString);

	/**
	 * Plays a turn of the game, increasing the turn counter and returning the score of the player's input. 
	 * If each of the characters of the provided input is a bull, the game is marked as won.
	 *
	 * @param Input The valid(!) input to check against the hidden word.
	 * @return The score of the input, as a FBullCowStruct.
	 */
	FBullCowStruct SubmitValidGuess(FString);

	// Stateless operations
	FText GetIntroductionText() const;
	int32 GetCurrentNumberOfTries() const;
	int32 GetMaximumNumberOfTries() const;

	/**
	 * Checks if the game is still in progress.
	 *
	 * @return true if the game is not won and the current number of tries does not exceed the maximum number of tries.
	 */
	bool IsGameInProgress() const;
	bool IsGameWon() const;
	EInputValidity ValidateInput(FString) const;

private:
	// Helper methods
	bool IsOfDifferentLength(FString) const;
	bool IsNotAnIsogram(FString) const;
	bool IsNotLowercaseLatin(FString) const;

	// Private fields
	bool m_IsGameWon;
	FString m_Isogram;
	int32 m_CurrentTries;
	int32 m_MaxTries;
};
