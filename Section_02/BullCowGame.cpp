#include "pch.h"
#include "BullCowGame.h"

#include <iostream>
#include <cctype>
#include <unordered_set>
#include <string>

// Aliasing to make things look a little more Unreal
using FString = std::string;
using FText = std::string;
using int32 = int;
template<class T> using TSet = std::unordered_set<T>;

constexpr int32 MAX_TRIES = 5;
constexpr const char* ISOGRAM = "isogram";

FBullCowGame::FBullCowGame()
	: FBullCowGame(FString(ISOGRAM))
{ }

FBullCowGame::FBullCowGame(FString Isogram)
{
	m_CurrentTries = 1;
	m_MaxTries = MAX_TRIES;
	m_Isogram = Isogram;
	m_IsGameWon = false;
}

FBullCowStruct FBullCowGame::SubmitValidGuess(FString Input)
{
	FBullCowStruct BullCowStruct = FBullCowStruct();
	for (int32 i = 0; i < (int32)m_Isogram.length(); i++)
	{
		// Check if the character is a bull
		if (m_Isogram[i] == Input[i])
		{
			BullCowStruct.Bulls++;
		}
		// If it is not, check if it is a cow
		else if (m_Isogram.find(Input[i]) != FString::npos)
		{
			BullCowStruct.Cows++;
		}
	}
	if (BullCowStruct.Bulls == (int32)m_Isogram.length())
	{
		m_IsGameWon = true;
	}

	// Ensure we increment the turn counter
	m_CurrentTries++;

	return BullCowStruct;
}

FText FBullCowGame::GetIntroductionText() const
{
	FText IntroductionText = "Welcome to Bulls and Cows!\n";
	IntroductionText += FText("Can you guess the ") += std::to_string((int32) m_Isogram.length()) += " letter word?\n";
	IntroductionText += FText("Only valid guesses count!\n");
	IntroductionText += FText("In order to be valid, a guess must be:\n");
	IntroductionText += FText("\t-an isogram.\n");
	IntroductionText += FText("\t-made up of all lowercase latin characters.\n");
	IntroductionText += FText("\t-of the same length as the word you're trying to guess.\n");
	IntroductionText += FText("\nGot all that? Let's go!\n");
	return IntroductionText;
}

int32 FBullCowGame::GetCurrentNumberOfTries() const
{
	return m_CurrentTries;
}

int32 FBullCowGame::GetMaximumNumberOfTries() const
{
	return m_MaxTries;
}

bool FBullCowGame::IsGameInProgress() const
{
	if (m_IsGameWon || (m_CurrentTries > m_MaxTries)) 
	{
		return false;
	}
	return true;
}

bool FBullCowGame::IsGameWon() const
{
	return m_IsGameWon;
}

EInputValidity FBullCowGame::ValidateInput(FString Input) const
{
	// Check if the input differs in length
	if (IsOfDifferentLength(Input))
	{
		return EInputValidity::LENGTH_DIFFERENCE;
	}
	// Check if the input is an isogram
	if (IsNotAnIsogram(Input))
	{
		return EInputValidity::NOT_ISOGRAM;
	}
	// Check if the input is in lower case latin
	if (IsNotLowercaseLatin(Input))
	{
		return EInputValidity::NOT_LOWERCASE_LATIN;
	}

	return EInputValidity::OK;
}

bool FBullCowGame::IsOfDifferentLength(FString Input) const
{
	if (Input.length() == m_Isogram.length())
	{
		return false;
	}
	return true;
}

bool FBullCowGame::IsNotAnIsogram(FString Input) const
{
	// Use set for constant lookup and insertion times.
	TSet<char> CharSet;
	for (char Character : Input)
	{
		char LowerCasedChar = tolower(Character);
		// Check if the character has been looped over before.
		if (CharSet.find(LowerCasedChar) != CharSet.end())
		{
			return true;
		}

		// Keep track of the characters looped over.
		CharSet.insert(LowerCasedChar);
	}
	return false;
}

bool FBullCowGame::IsNotLowercaseLatin(FString Input) const
{
	for (char Character : Input)
	{
		if (Character < 'a' || Character > 'z')
		{
			return true;
		}
	}
	return false;
}